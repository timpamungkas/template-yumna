# Build command
#
# 1. gradle clean bootJar
# 2. docker build --label template --tag template . 
# 3. docker login
# 4. docker tag template timpamungkas/template
# 5. docker push timpamungkas/template
# gradle clean bootJar && docker build --label template --tag template:1.0 . && docker tag template:1.0 timpamungkas/template:1.0 && docker push timpamungkas/template:1.0

# Start with a base image containing Java runtime
FROM openjdk:11-jre-stretch	

# Add Maintainer Info
LABEL maintainer="timotius.pamungkas@bfi.co.id"

# put yml here for external config
RUN mkdir conf

# Add a volume pointing to /tmp
VOLUME /tmp

# Make port 8002 available to the world outside this container
EXPOSE 9001

# The application's jar file
ARG JAR_FILE=build/libs/template-1.0.jar

# Add the application's jar to the container
ADD ${JAR_FILE} template.jar

# Run the jar file 
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/template.jar"]