1. Import project as gradle (e.g. from Eclipse > File > Import > Existing Gradle Project)
2. Config external ada di common.config.TemplateExternalConfig. Pada dasarnya, ini meng-nform spring: "Pakai file config yang mana selain default spring"
3. Abaikan YamlPropertySourceFactory kalau pakai .properties file. Berhubung saya pakai yaml, ini harus ada
4. Run TemplateApplication.java
5. Access http://localhost:9001
6. Will output : String value : value-one, value-three (value nya dari template.yml)


* Package as docker image (script ada di dockerfile)  
* Run docker container, tapi sekarang bind template.yml ke file external (e.g. D:\ansible\template.yml)
* Cek sample docker-compose.yml file untuk run
* Atau bisa pakai docker run biasa. See parameter -v untuk file binding, dan -p untuk port binding
  `docker run -d --name bfi-template -p 9001:9001 -v d:/ansible/template.yml:/conf/template.yml timpamungkas/template:1.0`
  

  
Kalau saya, on developer side:
1. conf/template.yml ini di ignore dari git, jadi ga pernah di commit ke git
2. punya satu snippet yang isinya key-value yang dipakai (E.g database password).
3. all credentials dimasukkan ke template.yml, bukan application.yml (bawaan spring)
4. Kalau untuk kerja, inform ke developer, snippet di no 2. Minta developer nya tarik manual dari snippet itu (karena di ignore di git)
5. Ke depannya maintain all credential ga pake proses ini, tapi pakai digital vault via CI/CD pipeline