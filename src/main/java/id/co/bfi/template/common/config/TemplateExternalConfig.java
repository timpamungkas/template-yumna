package id.co.bfi.template.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@PropertySources({ @PropertySource(factory = YamlPropertySourceFactory.class, value = { "classpath:template.yml",
		"file:/conf/template.yml", "file:c:/conf/template.yml" }, ignoreResourceNotFound = true) })
public class TemplateExternalConfig {
}
