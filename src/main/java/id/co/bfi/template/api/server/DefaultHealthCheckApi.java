package id.co.bfi.template.api.server;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DefaultHealthCheckApi {

	@Value("${template.key-one:default-key}")
	private String keyOne;

	@Value("${template.sub-template.key-three:default-key}")
	private String keyThree;

	@GetMapping(value = { "/", "/ping", "/health" }, produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> ping() {
		return ResponseEntity.ok("String value : " + keyOne + ", " + keyThree);
	}

}
